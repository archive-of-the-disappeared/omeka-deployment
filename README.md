# omeka-deployment
A `docker-compose` file to setup an [Omeka-Classic](https://omeka.org/classic/) installation on your own server. 

## Usage
To use this file, make sure that you have a machine with `docker-compose` [installed](https://docs.docker.com/compose/install/) correctly, then:

- Clone this repository 
- Rename the `.env.example` to `.env`, and make the appropriate changes (change the password, etc...)
- Run `docker-compose up`. 
- Go to `localhost`, you will see the site setup page. Fill the form out to get the correct website configuration. 

That's it!

## Extras 

### Maximum file size

The maximum file size is set in line Omeka/Dockerfile. It is currently set to 64M. Customize as required. 

### Installed plugins

I currently install the following plugins by defult: 

- API import 
- ElementTypes
- Dropbox
- Simple Vocab
- Collection Tree
- Export


### Omeka API import

The Omeka API import plugin is in the plugins directory for you to use if you choose to. 

### Setting up HTTPS

This website is heavily inspired by [wmnnd/nginx-certbot](https://github.com/wmnnd/nginx-certbot/). Follow the directions in that repository to set up HTTPS for your domain.

### Error logs

I log any errors to `/var/www/html/application/logs/errors.log`


