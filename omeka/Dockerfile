FROM php:7.3-apache

# install required packages
RUN apt-get update && apt-get install -y unzip gettext-base imagemagick

# install required PHP extensions 
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli 
RUN docker-php-ext-install exif && docker-php-ext-enable exif

# make sure that the httpd rewrite module works 
RUN a2enmod rewrite

# Set the production settings 
RUN cp $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini
RUN echo 'upload_max_filesize = 64M' >> /usr/local/etc/php/conf.d/omkea.ini

# add Omeka as the root of the apache installation
ADD https://github.com/omeka/Omeka/releases/download/v2.7/omeka-2.7.zip /home
RUN unzip /home/omeka-2.7.zip -d /var/www/
WORKDIR /var/www
RUN rm -rf html
RUN mv omeka-2.7 html

# copy the db.ini template file, which gets the env vars substituted later through docker-compose comand
COPY db.ini.template /home 

# make sure that Omeka has write access to the files folder
RUN chown -R www-data:www-data /var/www/html/files

# add the Omeka API import plugin 
ADD https://github.com/omeka/plugin-OmekaApiImport/releases/download/v1.1.2/OmekaApiImport.zip /home
RUN unzip /home/OmekaApiImport.zip -d /var/www/html/plugins

# add the Select Owner plugin plugin 
ADD https://github.com/BGSU-LITS/omeka-plugin-SelectOwner/releases/download/v1.1/SelectOwner-1.1.zip /home
RUN unzip /home/SelectOwner-1.1.zip -d /var/www/html/plugins

# add the ElementTypes plugin 
ADD https://github.com/biblibre/omeka-plugin-ElementTypes/releases/download/v0.5.0/ElementTypes-0.5.0.zip /home
RUN unzip /home/ElementTypes-0.5.0.zip -d /var/www/html/plugins

# add the Dropbox plugin 
ADD https://github.com/omeka/plugin-Dropbox/releases/download/v0.7.2/Dropbox-0.7.2.zip /home
RUN unzip /home/Dropbox-0.7.2.zip -d /var/www/html/plugins
RUN chown -R www-data:www-data /var/www/html/plugins/Dropbox/files

# add the Simple Vocab plugin 
ADD https://github.com/omeka/plugin-SimpleVocab/releases/download/v2.2.2/SimpleVocab-2.2.2.zip /home
RUN unzip /home/SimpleVocab-2.2.2.zip -d /var/www/html/plugins

# add the CSV Import plugin 
ADD https://github.com/omeka/plugin-CsvImport/releases/download/v2.0.4/CsvImport-2.0.4.zip /home
RUN unzip /home/CsvImport-2.0.4.zip -d /var/www/html/plugins

# add the Collection Tree plugin 
ADD https://github.com/omeka/plugin-CollectionTree/releases/download/v2.1/CollectionTree-2.1.zip /home
RUN unzip /home/CollectionTree-2.1.zip -d /var/www/html/plugins

# add the Export plugin 
ADD https://github.com/biblibre/omeka-plugin-Export/releases/download/v0.1.0/Export-0.1.0.zip /home
RUN unzip /home/Export-0.1.0.zip -d /var/www/html/plugins

ADD https://github.com/omeka/plugin-DublinCoreExtended/releases/download/v2.2/DublinCoreExtended-2.2.zip /home
RUN unzip /home/DublinCoreExtended-2.2.zip -d /var/www/html/plugins

# enable logging
RUN chown -R www-data:www-data /var/www/html/application/logs/errors.log
RUN sed -i 's/^log.errors = false/log.errors = true/g' /var/www/html/application/config/config.ini
# increase allowd file size to 100mb TODO make this configurable using env vars 
RUN sed -i "/;upload.maxFileSize/c\upload.maxFileSize = \"100M\"" /var/www/html/application/config/config.ini
RUN echo "php_value upload_max_filesize 100M" >> /var/www/html/.htaccess
RUN echo "php_value post_max_size 110M" >> /var/www/html/.htaccess

# start in the right directory
WORKDIR /var/www/html/